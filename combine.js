const fs = require('fs')
const path = require('path')
const outPath = require('./dataPaths').out

const branches = fs.readdirSync(outPath).filter(folder => {
  return fs.lstatSync(path.join(outPath, folder)).isDirectory()
})

branches.forEach(branch => {
  const branchPath = path.join(outPath, branch)

  const dir = fs
    .readdirSync(branchPath)
    .filter(file => /^\d{4}(-\d{2}){2}\.csv$/.test(file))

  let out = []

  dir.forEach((file, index) => {
    const filePath = path.join(branchPath, file)
    let fileContents = fs.readFileSync(filePath).toString()
    if (index) {
      fileContents = fileContents
        .split('\n')
        .slice(1)
        .join('\n')
    }
    out.push(fileContents)
  })

  fs.writeFileSync(path.join(branchPath, 'combined.csv'), out.join('\n'))
})

const bigOut = branches.map((branch, index) => {
  const branchCombinedPath = path.join(outPath, branch, 'combined.csv')

  let branchCombinedRows = fs
    .readFileSync(branchCombinedPath)
    .toString()
    .split('\n')

  let rowsWithoutHeader = branchCombinedRows.slice(1)

  let result = []

  if (!index) {
    let splitHeader = branchCombinedRows[0].split(',')
    result.push([splitHeader[0], 'Oddział', ...splitHeader.slice(1)].join(','))
  }

  rowsWithoutHeader.forEach(row => {
    let splitRow = row.split(',')
    result.push([splitRow[0], branch, ...splitRow.slice(1)].join(','))
  })

  return result.join('\n')
})

fs.writeFileSync(
  path.join(outPath, 'combined.csv'),
  '\ufeff' + bigOut.join('\n')
)
