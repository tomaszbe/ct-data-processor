const acrissToRmsMap = require('../data/acrissToRmsMap')

const acrissToRms = acriss => {
  const result = acrissToRmsMap[acriss]
  if (result === undefined) {
    throw 'Nieznany acriss: ' + acriss
  }
  return result
}

module.exports = acrissToRms