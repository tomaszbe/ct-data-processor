const fs = require('fs')
const readline = require('readline')
const acrissToRms = require('./acrissToRms')

const toResultSets = (lines, headers) => {
  return lines.map(line => {
    return line.reduce((acc, cur, index) => {
      acc[headers[index]] = cur
      return acc
    }, {})
  }).filter(item => {
    return !!item.Acriss
  }).reduce((acc, cur) => {
    let resultSet = acc.find(item => item.Id === cur['#'])
    if (resultSet === undefined) {
      resultSet = {
        Id: cur['#'],
        DaysAhead: cur['Days Ahead'],
        Duration: cur['Duration'],
        offers: []
      }
      acc.push(resultSet)
    }


    let rmsClass

    try {
      rmsClass = acrissToRms(cur.Acriss)
    } catch(e) {
      throw e + ' - ' + cur.Model
    }

    if (rmsClass !== '') {

      resultSet.offers.push({
        Vendor: cur.Vendor,
        Class: rmsClass,
        Size: parseInt(cur.Size),
        TotalCharge: parseFloat(cur.TotalCharge)
      })
    }

    return acc

  }, [])
}

const parseCsv = async filePath => {

  const lineReader = readline.createInterface(
    fs.createReadStream(filePath)
  )

  let lines = []

  lineReader.on('line', line => lines.push(line.split(';')))

  await new Promise(resolve => {
    lineReader.on('close', resolve)
  })

  const headers = lines[0]
  lines = lines.slice(1)

  const resultSets = toResultSets(lines, headers)

  return resultSets
}

const parseCsvSync = filePath => {
  let lines = []
  const fileContents = fs.readFileSync(filePath).toString().split('\n')
  for (const row of fileContents) {
    lines.push(row.split(';'))
  }

  const headers = lines[0]
  lines = lines.slice(1)

  const resultSets = toResultSets(lines, headers)

  return resultSets
}

module.exports = parseCsvSync