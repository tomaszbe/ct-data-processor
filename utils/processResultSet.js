const processOffers = require('./processOffers')

const processResultSet = resultSet => {
  const classes = resultSet.offers.reduce((acc, cur) => {
    let container = acc.find(item => item.Class === cur.Class)
    if (container === undefined) {
      container = {
        Class: cur.Class,
        offers: []
      }
      acc.push(container)
    }

    container.offers.push({
      Vendor: cur.Vendor,
      TotalCharge: cur.TotalCharge
    })

    return acc
  }, [])

  classes.forEach(item => {
    item.result = processOffers(item.offers)
  })

  resultSet.offers = classes

}

module.exports = processResultSet