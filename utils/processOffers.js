const processOffers = offers => {
  offers.sort((a, b) => a.TotalCharge - b.TotalCharge)

  let ourPlace = 0
  let ourPrice
  let theirVendor = null
  let theirPrice

  for (let i = 0; i < offers.length; ++i) {
    const offer = offers[i]
    if (offer.Vendor === 'EXPRESS') {
      ourPlace = i + 1
      ourPrice = offer.TotalCharge
    } else if (theirVendor === null) {
      theirVendor = offer.Vendor
      theirPrice = offer.TotalCharge
    }

    if (theirVendor !== null && ourPlace > 0) {
      break
    }
  }

  return {
    ourPrice,
    theirPrice,
    ourPlace,
    theirVendor
  }
}

module.exports = processOffers
