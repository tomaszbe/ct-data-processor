const parseCsv = require('./utils/parseCsv')
const processResultSet = require('./utils/processResultSet')
const fs = require('fs')
const path = require('path')
const moment = require('moment')

const columnSeparator = ','

const headers = [
  'Data badania',
  'Data poczatku najmu',
  'Klasa',
  'Wyprzedzenie',
  'Dlugosc najmu',
  'Nasza pozycja',
  'Nasza cena',
  'Ich cena',
  'Najtanszy konkurent'
]

const headerLine = headers.join(columnSeparator)

const { in: inPath, out: outPath } = require('./dataPaths')

function workFile(filePath, dateString, branch) {
  const date = moment(dateString)
  const data = parseCsv(filePath)
  let outLines = [headerLine]
  data.forEach(set => {
    processResultSet(set)
    set.offers.forEach(category => {
      let line = [
        dateString,
        date
          .clone()
          .add(set.DaysAhead, 'days')
          .format('Y-MM-DD'),
        category.Class,
        set.DaysAhead,
        set.Duration,
        category.result.ourPlace,
        category.result.ourPrice,
        category.result.theirPrice,
        category.result.theirVendor
      ].join(columnSeparator)
      outLines.push(line)
    })
  })
  if (!fs.existsSync(path.join(outPath, branch))) {
    fs.mkdirSync(path.join(outPath, branch))
  }
  fs.writeFileSync(
    path.join(outPath, branch, dateString + '.csv'),
    outLines.join('\n')
  )
}

const isDate = maybeDate => /^\d{4}(-\d{2}){2}$/.test(maybeDate)
const isCsv = maybeCsv => path.extname(maybeCsv) === '.csv'

fs.readdirSync(inPath)
  .filter(isDate)
  .forEach(date => {
    fs.readdirSync(path.join(inPath, date))
      .filter(isCsv)
      .forEach(branch => {
        const filePath = path.join(inPath, date, branch)
        workFile(filePath, date, path.basename(branch, '.csv'))
      })
  })
