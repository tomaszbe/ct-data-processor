module.exports = {
  CCAR: 'C',
  CCMR: 'C',
  CDAH: 'C+',
  CDAR: 'C',
  CDMD: 'C+',
  CDMN: 'C',
  CDMR: 'C',
  CFAR: 'SUV',
  CFMR: 'SUV',
  CKMD: 'M',
  CLMR: 'C',
  CMMV: 'M',
  CWAR: '',
  CWMD: '',
  CWMN: '',
  CWMR: '',
  DDAR: 'C',
  ECMR: 'B',
  EDAR: 'B',
  EDMN: 'B',
  EDMR: 'B',
  EDMV: 'B',
  EFAR: 'SUV',
  EFMR: 'SUV',
  EGAR: 'SUV',
  EWAR: '',
  EWMR: '',
  FCAR: 'E',
  FDAR: 'E',
  FDMR: 'E',
  FVAR: 'D7',
  FVMR: 'D7',
  FWAR: '',
  ICMN: 'D',
  IDAN: 'D+',
  IDAR: 'D+',
  IDMR: 'D',
  IFAH: 'SUV',
  IFAR: 'SUV',
  IFMR: 'SUV',
  IKMN: 'P',
  ITMR: '',
  IVAR: 'D7',
  IVMD: 'D7',
  IVMR: 'D7',
  IWAR: '',
  IWMD: '',
  IWMN: '',
  IWMR: '',
  JFAR: 'SUV',
  LDAD: 'E',
  LDAR: 'E',
  LFAD: 'E',
  MBMR: 'A+',
  MDAR: '',
  MCMR: 'A+',
  MDMR: 'A+',
  PDAD: 'E',
  PDAH: 'E',
  PDAR: 'E',
  PDMR: 'E',
  PFAD: 'SUV',
  PFAR: 'SUV',
  PWAR: '',
  SDAR: 'D+',
  SDMD: 'D',
  SDMR: 'D',
  SFAR: 'SUV',
  SKMD: 'P',
  SKMN: 'P',
  SVMD: 'R',
  SVMR: 'R',
  SWAR: '',
  SWMR: '',
  VPIW: 'M',
  PCAR: ''
}
